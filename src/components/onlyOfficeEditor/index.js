import React from 'react';
import { DocumentEditor } from "@onlyoffice/document-editor-react";
import './index.less';

//https://api.onlyoffice.com/zh/editors/callback
//https://api.onlyoffice.com/editors/config/
const OnlyOfficeEditor = () => {

    const onDocumentReady = (event) => {
        // 请输入函数体代码
        console.log("Document is loaded", event);
    };

    return (
        <div className='docx-editor-wrapper'>

            <DocumentEditor
                id="docxEditor"
                documentServerUrl="http://117.72.41.151:9050"
                config={{
                    "document": {
                        "fileType": "docx",
                        "key": "wzy123"+ new Date().getMilliseconds(),
                        "title": "Example Document Title.docx",
                        "url": "http://117.72.41.151:528/getFile",
                        "permissions": {
                            "comment": true,
                            "copy": true,
                            "download": true,
                            "edit": true,
                            "print": true,
                            "fillForms": true,
                            "modifyFilter": true,
                            "modifyContentControl": true,
                            "review": true
                        }
                    },
                    "documentType": "word",
                    //"type": "mobile",
                    "editorConfig": {
                        "mode": "edit",
                        "lang": "zh-CN", //中文还是英文
                        "callbackUrl": "http://117.72.41.151:528/callback",
                        "user": {
                            "c": "liu",
                            "name": "liu"
                        },
                        "forcesave": true
                    },
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlZGl0b3JDb25maWciOnsibW9kZSI6ImVkaXQiLCJjYWxsYmFja1VybCI6Imh0dHA6Ly8xMTcuNzIuNDEuMTUxOjUyOC9jYWxsYmFjayIsImxhbmciOiJ6aC1DTiIsInVzZXIiOnsibmFtZSI6ImxpdSIsImlkIjoibGl1In19LCJkb2N1bWVudFR5cGUiOiJ3b3JkIiwiZG9jdW1lbnQiOnsicGVybWlzc2lvbnMiOnsibW9kaWZ5Q29udGVudENvbnRyb2wiOnRydWUsImRvd25sb2FkIjp0cnVlLCJwcmludCI6dHJ1ZSwibW9kaWZ5RmlsdGVyIjp0cnVlLCJlZGl0Ijp0cnVlLCJmaWxsRm9ybXMiOnRydWUsInJldmlldyI6dHJ1ZSwiY29tbWVudCI6dHJ1ZSwiY29weSI6dHJ1ZX0sInRpdGxlIjoiRXhhbXBsZSBEb2N1bWVudCBUaXRsZS5kb2N4IiwiZmlsZVR5cGUiOiJkb2N4Iiwia2V5Ijoid3p5MTIzIiwidXJsIjoiaHR0cDovLzExNy43Mi40MS4xNTE6NTI4L2dldEZpbGUifX0.yQVY3FRz7uet7AT7VJ50GdNUaxNq7921b0ORzFejTnc"
                }}
                events_onDocumentReady={onDocumentReady}
            />
        </div>
    );
};

export default OnlyOfficeEditor;